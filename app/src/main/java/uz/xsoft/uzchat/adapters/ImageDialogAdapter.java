package uz.xsoft.uzchat.adapters;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import uz.xsoft.uzchat.R;
import uz.xsoft.uzchat.listeners.ImageDialogListener;

public class ImageDialogAdapter extends RecyclerView.Adapter<ImageDialogAdapter.ImageHolder> {

    List<Bitmap> list;
    ImageDialogListener imageDialogListener;

    public ImageDialogAdapter(List<Bitmap> list, ImageDialogListener listener) {
        this.list = list;
        imageDialogListener = listener;
    }

    @NonNull
    @Override
    public ImageHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_item, parent, false);
        return new ImageHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ImageHolder extends RecyclerView.ViewHolder {

        ImageView imageView;

        public ImageHolder(@NonNull View v) {
            super(v);
            imageView = v.findViewById(R.id.imageView_dialog_item);
        }

        public void bind(final int i) {
            imageView.setImageBitmap(list.get(i));

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    imageDialogListener.sendData(i);
                }
            });
        }
    }
}
