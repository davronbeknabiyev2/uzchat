package uz.xsoft.uzchat.ui.fragment;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.tapadoo.alerter.Alert;
import com.tapadoo.alerter.Alerter;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import uz.xsoft.uzchat.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "TTT";

    TextInputEditText login;
    TextInputEditText password;

    Button singIn;
    Button newAccount;
    Context context;

    NavController navController;

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

        SharedPreferences preferences = context.getSharedPreferences("Users", Context.MODE_PRIVATE);
        boolean isEnter = preferences.getBoolean("isEnter", false);

        if (isEnter) {
            navController.navigate(R.id.action_loginFragment_to_mainFragment);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController = Navigation.findNavController(view);

        login = view.findViewWithTag(R.id.edit_login_main);
        password = view.findViewWithTag(R.id.edit_parol_main);

        singIn = view.findViewById(R.id.login_main);
        newAccount = view.findViewById(R.id.new_account_main);


        singIn.setOnClickListener(this);
        newAccount.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.login_main) {
            Toast.makeText(getContext(), "hi", Toast.LENGTH_SHORT).show();

        } else if (view.getId() == newAccount.getId()) {
//            NavController controller=getParentFragment();
            navController.navigate(R.id.action_loginFragment_to_newAccountFragment);
        }
    }
}
