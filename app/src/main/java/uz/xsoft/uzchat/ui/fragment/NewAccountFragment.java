package uz.xsoft.uzchat.ui.fragment;


import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.UserHandle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import uz.xsoft.uzchat.R;
import uz.xsoft.uzchat.adapters.ImageDialogAdapter;
import uz.xsoft.uzchat.listeners.ImageDialogListener;
import uz.xsoft.uzchat.models.UserModel;


/**
 * A simple {@link Fragment} subclass.
 */
public class NewAccountFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "TTT";

    TextInputEditText name;
    TextInputEditText phone;
    TextInputEditText login;
    TextInputEditText password;
    Button singUp;
    Button close;
    ImageView iconUser;

    Bitmap bitmapMain;

    DatabaseReference databaseReference;

    int imageId;


    public NewAccountFragment() {
        // Required empty public constructor
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        databaseReference = FirebaseDatabase.getInstance().getReference("users");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_new_account, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        name = view.findViewById(R.id.edit_name_new);
        phone = view.findViewById(R.id.edit_mub_num_new);
        login = view.findViewById(R.id.edit_login_new);
        password = view.findViewById(R.id.edit_password_new);

        singUp = view.findViewById(R.id.sing_up_new);
        close = view.findViewById(R.id.close_new);
        iconUser = view.findViewById(R.id.icon_image);
        try {
            InputStream inputStream = getContext().getAssets().open("icons/icon" + 3 + ".png");
            bitmapMain = BitmapFactory.decodeStream(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        iconUser.setImageBitmap(bitmapMain);
        singUp.setOnClickListener(this);
        close.setOnClickListener(this);
        iconUser.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.sing_up_new) {
            saveUser();
        } else if (view.getId() == R.id.close_new) {
            getFragmentManager().popBackStack();
        } else if (view.getId() == R.id.icon_image) {
            showDialog();
        }
    }

    public void saveUser() {
        String nameUser = toStringText(name);
        String phoneUser = toStringText(phone);
        String loginUser = toStringText(login);
        String passUser = toStringText(password);
        int imageId = this.imageId;
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("users");
        String userId = databaseReference.push().getKey();

        if (nameUser.isEmpty() || phoneUser.isEmpty() || loginUser.isEmpty() || passUser.isEmpty()) {
            return;
        } else {

            UserModel userModel = new UserModel(
                    userId,
                    nameUser,
                    phoneUser,
                    loginUser,
                    passUser,
                    imageId
            );

            databaseReference.child(userId).setValue(userModel);
            Toast.makeText(getContext(), "You are add list!", Toast.LENGTH_SHORT).show();

            SharedPreferences preferences = getActivity().getSharedPreferences("Users", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();

            editor.putString("user_id", userId);
            editor.putBoolean("isEnter", true);

            editor.apply();

        }
    }


    public void showDialog() {
        final List<Bitmap> list = new ArrayList<>();
        InputStream inputStream;
        for (int i = 1; i <= 21; i++) {
            try {
                inputStream = getContext().getAssets().open("icons/icon" + i + ".png");
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
//                Log.d(TAG, "onClick: " + bitmap.toString());
                list.add(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        bitmapMain = list.get(2);

        final AlertDialog.Builder aBuilder = new AlertDialog.Builder(getContext());


        View view = getActivity().getLayoutInflater().inflate(R.layout.icon_dioalog, null);
        aBuilder.setView(view);

        RecyclerView recyclerView = view.findViewById(R.id.rec_dialog_icon);
        Button button = view.findViewById(R.id.but_dialog_icon);
        final ImageView imageView = view.findViewById(R.id.image_dialog_icon);

        imageView.setImageBitmap(bitmapMain);

        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));

        ImageDialogAdapter imageDialogAdapter = new ImageDialogAdapter(list,
                new ImageDialogListener() {
                    @Override
                    public void sendData(int i) {
                        bitmapMain = list.get(i);
                        imageView.setImageBitmap(bitmapMain);
                    }
                });

        recyclerView.setAdapter(imageDialogAdapter);

        final AlertDialog alertDialog = aBuilder.create();
        alertDialog.show();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iconUser.setImageBitmap(bitmapMain);
                alertDialog.dismiss();
            }
        });


    }

    public String toStringText(EditText editText) {
        if (editText.getText().toString().isEmpty()) {
            editText.setError(editText.getHint().toString() + " is empty.");
            return "";
        }
        return editText.getText().toString();
    }
}


